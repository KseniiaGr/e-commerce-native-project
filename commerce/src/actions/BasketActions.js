import { ADD_TO_BASKET, REMOVE_FROM_BASKET } from "./types";
import { Toast } from "native-base";

export const addToBasket = product => dispatch => {
  Toast.show({
    text: `Item added ${product.name}`,
    duration: 3000
  });
  dispatch({
    type: ADD_TO_BASKET,
    payload: product
  });
};

export const removeFromBasket = product => dispatch => {
  Toast.show({
    text: `${product.name} removed from basket`,
    duration: 3000
  });
  dispatch({
    type: REMOVE_FROM_BASKET,
    payload: product
  });
};

// const newState = { ...state }
// _.remove(newState.basket, (item) => ((item.Code === action.item.Code) && (item.id === action.item.id)));
// newState.basketTotal = parseFloat(newState.basketTotal) - parseFloat(action.item.Price);
// return newState;
