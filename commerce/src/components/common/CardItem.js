import React from 'react';
import { View } from 'react-native';

const CardItem = (props) => {
    return (
        <View style={styles.containerStyle}>
            {props.children}
        </View>
    );
};

const styles = {
    containerStyle: {   
        borderBottomWidth: 1,
        padding: 5,
        backgroundColor: '#fff',
        justifyContext: 'flex-start',
        flexDirection: 'row',
        borderColor: '#D9FAFF',
        position: 'relative'
    }
};

export { CardItem };
