import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import HomePage from './components/HomePage';
import SignInScreen from './components/SignInScreen';
import Checkout from './components/Checkout';
import { Spinner } from './components/common/Spinner';

const AppNavigator = createStackNavigator(
    { 
        HomePage: { screen: HomePage }, 
        Checkout: { screen: Checkout }
    }
    );
const AuthNavigator = createStackNavigator(
    { SignIn: SignInScreen },
    {
      
    },
    );

export default createSwitchNavigator (
    {
        AuthLoading: Spinner,
        App: AppNavigator,
        Auth: AuthNavigator
    },
    {
        initialRouteName: 'Auth',
        
    }
)