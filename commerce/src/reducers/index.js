import { combineReducers } from 'redux';
import ProductsReducer from './ProductsReducer';
import SelectionReducer from './SelectionReducer';
import AuthReducer from './AuthReducer';
import BasketReducer from './BasketReducer';

export default combineReducers({
    products: ProductsReducer,
    selectedProductId: SelectionReducer,
    auth: AuthReducer,
    basket: BasketReducer
});