export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAIL = 'login_user_fail';
export const LOGIN_USER = 'login_user';
export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_changed';

export const PRODUCTS_FETCH = 'products_fetch';
export const ADD_TO_BASKET = 'add_to_basket';
export const REMOVE_FROM_BASKET = 'remove_from_basket';
