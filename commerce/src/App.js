import React, { Component } from 'react';
import { Root } from 'native-base'
import AppNavigator from './AppNavigator';
import firebase from 'firebase';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import ReduxThunk from 'redux-thunk';

export default class App extends Component {

  componentWillMount(){
    const config = {
        apiKey: "AIzaSyASkJUPdqnB6Anwisma0dXJYPOoPjzeR3Y",
        authDomain: "pastrythief-79c74.firebaseapp.com",
        databaseURL: "https://pastrythief-79c74.firebaseio.com",
        projectId: "pastrythief-79c74",
        storageBucket: "pastrythief-79c74.appspot.com",
        messagingSenderId: "86905544653"
      };
      firebase.initializeApp(config);
}

  render() {
    return (
      <Root>  
      <Provider store={createStore(reducers, {}, applyMiddleware(ReduxThunk))}>
        <AppNavigator/>
       </Provider>
      </Root>
    );
  }
}