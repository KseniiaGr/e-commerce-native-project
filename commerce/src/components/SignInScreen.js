import React, { Component } from 'react';
import { View, Image, Dimensions } from 'react-native';
import { Text, Item, Input, Button} from 'native-base';
import { Spinner } from './common';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions';


class SignInScreen extends Component {


static navigationOptions = {
    title: 'Please Sign In',
headerStyle: {
    backgroundColor: '#AAA5D1'
},
headerTitleStyle: {
    flex: 1,
    textAlign:'center',
    fontWeight: 'bold',
    fontSize: 25
  },
}

    onEmailChange(text) {
        const { emailChanged } = this.props;
        emailChanged(text);
    }

    onPasswordChange(text) {
        const { passwordChanged } = this.props;
        passwordChanged(text);
        
    }

    onButtonPress() {
        const { loginUser, email, password, navigation } = this.props;
        loginUser( email, password, navigation );
      
        }
    renderError() {
        const { error } = this.props;
        if(error) {
            return (
                <View>
                    <Text>
                        {error}
                    </Text>
                 </View>
                )
        }
    }

    renderButton(){
        const { loading } = this.props;
        const { inputStyle } = styles;
        if(loading) {
            return <Spinner size="large" />
        }
        return (
            <Button 
                full 
                style={inputStyle} 
                onPress={this.onButtonPress.bind(this)}
            >
            <Text>Sign In</Text>
            </Button>
        )
    }
    
    render() {
        const { email, password} = this.props;
        const { screenStyle, inputBoxStyle, textInputStyle, imageStyle, backgroundImage} = styles;
        return (
            <View style={screenStyle}> 
            <View style={inputBoxStyle}> 
                <Item rounded style={textInputStyle}>
                    <Input label='Email' placeholder="email@googlemail.com" onChangeText={this.onEmailChange.bind(this)} value={email}  />
                 </Item>
                 <Item rounded>
                    <Input label='Password' placeholder="password" onChangeText={this.onPasswordChange.bind(this)} secureTextEntry value={password} />
                 </Item>     
                {this.renderError()}
                {this.renderButton()}  
                </View>   
                <View style={imageStyle}>
                <Image source={require('../resources/images/pastry-drawing.png')} style={backgroundImage}/>
            </View>    
            </View>      
        )
    }

}

const styles = {
    screenStyle: {
        width: Dimensions.get('window').width * 1,
        height: Dimensions.get('window').height * 1,
        flex: 1
    },
    inputBoxStyle: {
        zIndex: 10,
        backgroundColor: 'white',
        margin: 15,
        padding: 15,
        borderRadius:10,
        borderWidth: 1,
        borderColor: '#AAA5D1'
    },
    inputStyle: {
        marginBottom: 20,
        marginTop: 20,
        backgroundColor: '#665990'
    },
    textInputStyle: {
        marginBottom: 20,
        marginTop: 20
    },
    imageStyle: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'flex-end'
    },
    backgroundImage: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height * 1,
        marginBottom: 10,
      } 
}

const mapStateToProps = ({ auth }) => {
    const { email, password, error, loading } = auth;
    return {email, password, error, loading };
}

export default connect(mapStateToProps, { emailChanged, passwordChanged, loginUser })(SignInScreen);