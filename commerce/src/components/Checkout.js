import React, { Component } from "react";
import { View, ScrollView, Dimensions, Image } from "react-native";
import { Item, Input, Label, Picker, Button, Text } from "native-base";
import Communications from "react-native-communications";

class Checkout extends Component {

  static navigationOptions = () => {
    return {
      headerTitle: "Checkout",
      headerStyle: {
        backgroundColor: "#AAA5D1",
      },
      headerTitleStyle:{
        flex: 1,
        textAlign:'center',
        marginRight: 60,
        fontWeight: 'bold',
        fontSize: 25
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      selected: "key0",
      phone: '',
      name: ''
    };
  }
  onValueChange(value: string) {
    this.setState({
      selected: value
    });
  }

  onButtonPress() {
      const { phone, name } = this.state;
      Communications.text(phone, `Thank you ${name}. Your order has been received and will be with you soon.`);
    
  }

  render() {
    const { inputStyle, screenStyle, imageStyle, backgroundImage, textInputStyle } = styles;
    const { phone, name } = this.state;
    return (
      <View style={screenStyle}>
      <ScrollView style={{zIndex:10}}>
        <View
          style={{
            zIndex: 10,
            backgroundColor: "white",
            margin: 15,
            padding: 15,
            borderRadius: 10,
            borderWidth: 1,
            borderColor: "#AAA5D1"
          }}
        >
          <Label style={{ fontWeight: "bold", color: "black" }}>
            Delivery Details
          </Label>
          <Item rounded style={textInputStyle}>
            <Input value={name} label="Name" placeholder="Full Name" onChangeText={value => this.setState({ name: value })} />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input label="HouseNumber" placeholder="House Number" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input label="FirstAddress" placeholder="First Line of Address" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input
              label="SecondAddress"
              placeholder="Second Line of Address "
            />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input label="City" placeholder="City" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input label="County" placeholder="County" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input label="Postcode" placeholder="Postcode" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input
              value={phone}
              label="Phone"
              placeholder="Mobile Number"
              onChangeText={value => this.setState({ phone: value })}
            />
          </Item>
        </View>
        <View
          style={{
            zIndex: 10,
            backgroundColor: "white",
            margin: 15,
            padding: 15,
            borderRadius: 10,

            borderWidth: 1,
            borderColor: "#AAA5D1"
          }}
        >
          <Label style={{ fontWeight: "bold", color: "black" }}>
            Payment Details
          </Label>
          <Item rounded style={textInputStyle}>
            <Input label="CardHolder" placeholder="Name as shown on Card" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input label="CVV" placeholder="CVV (3 digits on back of card)" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input label="CardNum" placeholder="Card Number" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Input label="ExpiryDate" placeholder="Expiry Date(mm/yy)" />
          </Item>
          <Item rounded style={textInputStyle}>
            <Picker
              mode="dropdown"
              selectedValue={this.state.selected}
              onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label="MasterCard" value="key0" />
              <Picker.Item label="Visa" value="key1" />
              <Picker.Item label="Visa Debit" value="key2" />
              <Picker.Item label="Maestro" value="key3" />
            </Picker>
          </Item>
          <Button 
          full 
          style={inputStyle} 
          onPress={() => this.onButtonPress()}>
          <Text>Confirm Order</Text>
          </Button>
        </View>
      </ScrollView>
        <View style={imageStyle}>
          <Image source={require('../resources/images/pastry-drawing.png')} style={backgroundImage}/>
        </View>  
      </View>
    );
  }
}

const styles = {
  screenStyle: {
    width: Dimensions.get('window').width * 1,
    height: Dimensions.get('window').height * 1,
    flex: 1
  },
  inputStyle: {
    marginBottom: 20,
    marginTop: 20,
    backgroundColor: "#665990"
  },
  textInputStyle: {
    marginBottom: 10,
    marginTop: 10
  },
  imageStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
},
backgroundImage: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height * 1,
    marginBottom: 10,
  } 
};

export default Checkout;
