import React, { Component } from "react";
import { CardItem, Card } from "./common";
import {
  Text,
  TouchableOpacity,
  LayoutAnimation,
  NativeModules,
  Image,
  View
} from "react-native";
import { connect } from "react-redux";
import * as actions from "../actions";
import { Button } from "native-base";

const { UIManager } = NativeModules;

UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

class ProductItem extends Component {
  componentWillUpdate() {
    LayoutAnimation.spring();
  }



  renderDescription() {
    const { product: { item }, expanded, addToBasket } = this.props;
    const {descriptionStyle, inputStyle} = styles
    if (expanded) {
      return (
        <CardItem>
        <View style={descriptionStyle}>
          <Text style={{ flex: 1, fontSize: 18 }}>
            {item.description}
          </Text>

          <Button
            full
            style={inputStyle}
            onPress={() => addToBasket(item)}
          >
            <Text style={{ fontSize: 18, color: 'white' }}>Add to Basket</Text>
          </Button>
          </View>
        </CardItem>
      );
    }
  }

  render() {
    const { titleStyle, imageStyle, thumbnailContainerStyle } = styles;
    const { id, name, image, price } = this.props.product.item;
    const newPrice = price.toFixed(2);

    return (
      <TouchableOpacity onPress={() => this.props.selectProduct(id)}>
        <Card>
          <CardItem>
            <View style={thumbnailContainerStyle}>
              <Text style={titleStyle}>{name}: £{newPrice}</Text>
            </View>
          </CardItem>
          <Image style={imageStyle} source={{ uri: image }} />
          {this.renderDescription()}
        </Card>
      </TouchableOpacity>
    );
  }
}

const styles = {
  titleStyle: {
    fontSize: 22,
    fontWeight: "bold"
  },
  thumbnailContainerStyle: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  imageStyle: {
    height: 300,
    flex: 1,
    width: null
  },
  inputStyle: {
    marginBottom: 20,
    marginTop: 20,
    backgroundColor: "#665990",
  },
  descriptionStyle:{
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between'
  }
};

const mapStateToProps = (state, ownProps) => {
  const expanded = state.selectedProductId === ownProps.product.item.id;

  return { expanded };
};

export default connect(
  mapStateToProps,
  actions
)(ProductItem);
