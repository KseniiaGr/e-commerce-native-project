import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  Modal,
  TouchableOpacity,
  FlatList,
  Alert
} from "react-native";
import ProductsList from "./ProductsList";
import { Button } from "native-base";
import * as actions from "../actions";
import { connect } from "react-redux";

class HomePage extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: "Our Menu",
      headerRight: (
        <TouchableOpacity
          onPress={() => navigation.state.params.toggleModal()}
        >
          <Image
            source={require("../resources/shoppingBasket.png")}
            style={{ width: 30, height: 30, marginRight: 10 }}
          />
        </TouchableOpacity>
      ),
      headerStyle: {
        backgroundColor: "#AAA5D1"
      },
      headerTitleStyle: {
        flex: 1,
        textAlign: "center",
        marginLeft: 50,
        fontWeight: "bold",
        fontSize: 25
      }
    };
  };

  state = {
    modalVisible: false
  };

  componentDidMount() {
    const { navigation } = this.props;
    navigation.setParams({ toggleModal: this.toggleModal.bind(this) });
  }

  toggleModal() {
    const { modalVisible } = this.state;
    this.setState({ modalVisible: !modalVisible });
  }

  navigateToCheckout() {
    const {
      basket: { basket },
      navigation: { navigate }
    } = this.props;
    if (basket.length <= 0) {
      Alert.alert(
        "Basket Empty",
        "Please add items to your basket",
        [{ text: "OK", onPress: () => console.log("Okay") }],
        { cancelable: true }
      );
      return;
    }
    navigate("Checkout");
    this.toggleModal();
  }

  renderModal() {
    const { removeFromBasket } = this.props;
    const {basket, total} = this.props.basket;
    const {headerStyle, textContainer, textStyle, totalStyle, inputStyle, textButtonStyle} = styles;
    return (
      <View>
        <Text style={headerStyle}>Your Basket:</Text>
        <View style={{ marginLeft: 20, marginRight: 20 }}>
          <FlatList
            data={basket}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item }) => (
              <View style={textContainer}>
                <TouchableOpacity
                 style={{marginRight:10}}
                 onPress={() => removeFromBasket(item)}>
                  <Image
                    source={require("../resources/delete.png")}
                    style={{ width: 10, height: 10, marginTop: 12 }}
                  />
                </TouchableOpacity>              
                  <Text style={textStyle}>{item.name} </Text>
                  <Text style={[textStyle, {flex: 1, textAlign: 'right'}]}>£{item.price.toFixed(2)}</Text>
              </View>
            )}
          />
          <View style={textContainer}>
            <Text style={totalStyle}>Total:</Text>
            <Text style={totalStyle}>
              £{total.toFixed(2)}
            </Text>
          </View>

          <Button
            full
            style={inputStyle}
            onPress={() => {
              this.toggleModal();
            }}
          >
            <Text style={textButtonStyle}>Cancel</Text>
          </Button>
          <Button
            full
            style={inputStyle}
            onPress={() => this.navigateToCheckout()}
          >
            <Text style={textButtonStyle}>Checkout</Text>
          </Button>
        </View>
      </View>
    );
  }

  render() {
    return (
      <View>
        <Modal
          animationType={"slide"}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log("Modal has been closed");
          }}
        >
          {this.renderModal()}
        </Modal>
        <ProductsList />
      </View>
    );
  }
}

const styles = {
  headerStyle: {
    height: 55,
    textAlign: "center",
    backgroundColor: "#AAA5D1",
    fontWeight: "bold",
    fontSize: 25,
    color: "black",
    padding: 10,
    elevation: 1
  },
  textStyle: {
    fontWeight: "bold",
    fontSize: 18,
    marginTop: 5,

  },
  textButtonStyle: {
    fontWeight: "bold",
    fontSize: 18,
    color: "white"
  },
  inputStyle: {
    marginTop: 20,
    backgroundColor: "#665990"
  },
  textContainer: {
    marginTop: 5,
    borderTopColor: "#665990",
    borderTopWidth: 1,
    flexDirection: 'row'
  },
  totalStyle: {
    fontWeight: "bold",
    fontSize: 18,
    marginTop: 5,
    color: "black"
  }
};

const mapStateToProps = state => {
  return {
    products: state.products,
    basket: state.basket
  };
};

export default connect(
  mapStateToProps,
  actions
)(HomePage);
