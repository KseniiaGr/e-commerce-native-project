import _ from 'lodash';
import { ADD_TO_BASKET, REMOVE_FROM_BASKET } from '../actions/types';

const INITIAL_STATE = {
  basket: [],
  total: 0,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_TO_BASKET:
      return {
        ...state,
        basket: [action.payload, ...state.basket],
        total: state.total + action.payload.price,
      };

     case REMOVE_FROM_BASKET:
      const newState = {...state}

      let newList = [];
      let removed = false;
      newState.basket.map(
        (item) => {
          if(item.id === action.payload.id && !removed ){
            removed = true;
          } else {
            newList.push(item);
          }
        }
      )


      return {
        ...state,
        basket:  newList,
        total: state.total - action.payload.price,
      };

    default:
      return state;
  }
};

