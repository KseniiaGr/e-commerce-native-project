import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { connect } from 'react-redux';

import ProductItem from './ProductItem';

class ProductsList extends Component {
    
    renderItem(product) {
        return <ProductItem product={product} />;
    }
    
    
    render() {
        const {products}=this.props;
        return (
            <View>
                { <FlatList
                    data={products}
                    renderItem={this.renderItem}
                    keyExtractor={(product) => `${product.id}`}
                    /> }
            </View>
            )
    }
}

const mapStateToProps = state => {
    return { products: state.products };
}

export default connect(mapStateToProps)(ProductsList);
